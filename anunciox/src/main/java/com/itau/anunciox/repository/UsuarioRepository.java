package com.itau.anunciox.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.anunciox.model.Usuario;

	
	public interface UsuarioRepository  extends CrudRepository<com.itau.anunciox.model.Usuario, Integer>{

	}


