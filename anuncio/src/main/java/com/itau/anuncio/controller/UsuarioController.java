package com.itau.anuncio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.anuncio.model.Usuario;
import com.itau.anuncio.repository.UsuarioXRepository;

@Controller
public class UsuarioController {
	
	@Autowired
	UsuarioXRepository usuarioRepository;
	
	@RequestMapping(path="/usuario", method=RequestMethod.GET)
	@ResponseBody
	public Usuario inserirUsuario(@RequestBody Usuario usuario) {
		return usuarioRepository.save(usuario);
	}


}
